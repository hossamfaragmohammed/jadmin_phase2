﻿using JAdminInterface.Models.UCCEntities;
using JAdminInterface.Models.UCCViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using JAdminInterface.Models;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using JAdminInterface.Shared;
using PagedList;


namespace JAdminInterface.Controllers
{

    [Authorize]
    public class RegisteredAgentsListController : Controller
    {
        private  UCCEntities db = new UCCEntities();



        // GET: RegisteredAgents
        public ActionResult Index(int? pageNumber)
        {

           
            var result = db.Database
                .SqlQuery<sp_registered_agents_list_Result>("sp_registered_agents_list")
                .ToList().ToPagedList(pageNumber ?? 1, 10);

            

            return View(result);
        }


        public ActionResult RegisteredAgentInterview(int? AgentID)
        {
            /*  if (AgetnID == null)
              {
                  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
              }
              */

           

            DEF_AGENT registeredAgent = db.DEF_AGENT.Where(a => a.ID == AgentID).First();


            return View(registeredAgent);
        }

       
        public ActionResult ScheduleAgentInterview(int? AgentID)
        {
            List<AspNetUser> adminUsers = db.AspNetUsers.Where(u => u.AspNetRoles.Any(r => r.Name.Equals("Admin"))).ToList();

            ViewData["Users"] = adminUsers;
            
            DEF_AGENT agentEntity = db.DEF_AGENT.Where(e => e.ID == AgentID).SingleOrDefault();
            
            ScheduleInterviewViewModel scheduleViewModel = new ScheduleInterviewViewModel();

            scheduleViewModel.agent = agentEntity;
            
            return View(scheduleViewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ScheduleAgentInterview(FormCollection Form)
        {
            // Schedule the interview .. 
            int AgentID = int.Parse(Form["agent.ID"]);
            string ScheduledToUserID = Form["ScheduleToUser"];

            string ScheduledByUserID = System.Web.HttpContext.Current.User.Identity.GetUserId();


            DateTime ScheduleDate  = DateTime.Parse(Form["ScheduleDate"]);

            AGENT_INTERVIEW interview = db.AGENT_INTERVIEW.Where(i => i.AgentId == AgentID).SingleOrDefault();
            interview.AGENT_INTERVIEW_STATUSES = db.AGENT_INTERVIEW_STATUSES.Where(s => s.InterviewStatusName.Equals("Scheduled")).SingleOrDefault();

            // First deactivate any previouse schdeules on this interview ..
            var previouseSchedules = interview.AGENT_INTERVIEW_SCHEDULES.Where(s => s.INTERVIEW_SCHEDULE_STATUS.ScheduleStatusName.Equals("Active Schedule")).ToList();
            foreach (AGENT_INTERVIEW_SCHEDULES schedule in previouseSchedules) {
                schedule.INTERVIEW_SCHEDULE_STATUS = db.INTERVIEW_SCHEDULE_STATUS.Single(x => x.ScheduleStatusName.Equals("Inactive Schedule"));
            }
            
            // Create a new shedule ..

            AGENT_INTERVIEW_SCHEDULES interviewSchedule = new AGENT_INTERVIEW_SCHEDULES();
            interviewSchedule.ScheduledBy = ScheduledByUserID;
            interviewSchedule.ScheduledTo = ScheduledToUserID;
            interviewSchedule.ScheduleDate = ScheduleDate;
            interviewSchedule.CreationDate = DateTime.Now;
            interviewSchedule.INTERVIEW_SCHEDULE_STATUS= db.INTERVIEW_SCHEDULE_STATUS.Where(s => s.ScheduleStatusName.Equals("Active Schedule")).SingleOrDefault();

            interview.AGENT_INTERVIEW_SCHEDULES.Add(interviewSchedule);

            try
            {
                db.SaveChanges();
                // send notification email to the admnin for training
                EmailNotificationSender.SendMail(
                            "hossam.farag.mohammed@gmail.com",
                            "hossam.farag.mohammed@gmail.com",
                            "Jathwa Notification, Agent Interview Schedule",
                            "Hi Mr. Admin, You have been assigned to a scheduled agent interview on: " + interviewSchedule.ScheduleDate.ToShortDateString(),
                            "HIGH");


                
            }
            catch (Exception e) {
                Console.Write(e.Message);
            }
            


            return RedirectToAction("AdminWelcomePage");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisteredAgentInterview(FormCollection Form)
        {
           
            // Prepare interview data ..
            int agentId = int.Parse(Form["ID"]);
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            string interviewStatus = "";

            DEF_AGENT agent = db.DEF_AGENT.Include(s => s.AGENT_SKILL).Where(a => a.ID == agentId).First();
            AspNetUser user = db.AspNetUsers.Where(u => u.Id == userId).First();

           
            string overall_score = "", evaluation_comments = "";


            if (ModelState.IsValid)
            {
                foreach (string key in Form.AllKeys)
                {

                    // Get list of skills and assigned scores ..
                    if (key.EndsWith("-score_text_box"))
                    {

                        string skillID = (key.Split(new char[] { '-' })[0]);


                        INTERVIEW_DETAILS interviewDetail = agent.AGENT_INTERVIEW.First().INTERVIEW_DETAILS.Where(s => s.SkillId == int.Parse(skillID)).First();
                        interviewDetail.Score = double.Parse(Form[key]);
                    }
                    else if (key.Equals("evaluation_text_box"))
                    {
                        overall_score = Form[key];
                    }
                    else if (key.Equals("comments_text_area"))
                    {
                        evaluation_comments = Form[key];
                    }
                    else if (key.Equals("button"))
                    {
                        interviewStatus = Form[key];
                        
                    }
                }

                AGENT_INTERVIEW agentInterview = agent.AGENT_INTERVIEW.First();

                agentInterview.AspNetUser = user;
                agentInterview.InterviewComments = evaluation_comments;

                agentInterview.AGENT_INTERVIEW_STATUSES = db.AGENT_INTERVIEW_STATUSES.Where(s => s.InterviewStatusName.Equals(interviewStatus)).SingleOrDefault();

                try
                {
                    db.SaveChanges();

                    // send notification email to the admnin for training
                    EmailNotificationSender.SendMail(
                            "hossam.farag.mohammed@gmail.com",
                            "hossam.farag.mohammed@gmail.com",
                            "Jathwa Notification, Agent Interview Status review",
                            "Hi Mr. Admin, A new agent interview is submitted to the system",
                            "HIGH");

                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                    throw;
                }



                if (interviewStatus.Equals("Accepted"))
                {
                   // agent.AGENT_STATUS = db.AGENT_STATUS.Single(s => s.AgentStatusName.Equals("RFT"));
                   

                    string sql = "exec sp_move_agent_skills @AgentID = @AgentID_Param";
                    
                    var parameters = new[]{
                        new SqlParameter("@AgentID_Param", agent.ID) };

                    db.Database.SqlQuery<object>(sql, parameters).ToList();

                    db.SaveChanges();

                    return RedirectToAction("AssignCallCenters", agentInterview);
                }
                else if (interviewStatus.Equals("Save & Reschedule")) {
                    return RedirectToAction("ScheduleAgentInterview", new { AgentID = agent.ID });
                }
                else
                {
                    //agent.AGENT_STATUS = db.AGENT_STATUS.Single(s => s.AgentStatusName.Equals("Rejected"));

                    //db.SaveChanges();
                    return RedirectToAction("AdminWelcomePage");
                }
                
               // return RedirectToAction("AssignCallCenters");

            }

            return RedirectToAction("AdminWelcomePage");



        }




        public ActionResult AssignCallCenters(AGENT_INTERVIEW interview)
        {
            // refetch from DB because related objects are not passed while calling th function

            interview = db.AGENT_INTERVIEW.Where(i => i.Id == interview.Id)
                                          .Include(s => s.AGENT_INTERVIEW_STATUSES)
                                          .Include(s => s.DEF_AGENT)
                                          .Include(s => s.INTERVIEW_DETAILS)
                                          .SingleOrDefault();



            IEnumerable<DEF_CC> cc_list = db.DEF_CC.ToArray();

            ViewData["CC_LIST"] = cc_list;

            return View(interview);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AssignCallCenters(FormCollection formCollection)
        {
            int AgentID = 0;
            AGENT_INTERVIEW agent_interview = null;
            if (ModelState.IsValid)
            {


                int interviewID;
                
                int[] cc_id_list;

                foreach (string key in formCollection.AllKeys)
                {
                    if (key.Equals("InterviewID"))
                    {

                        interviewID = int.Parse(formCollection[key]);

                        agent_interview = db.AGENT_INTERVIEW.Include(s => s.INTERVIEW_AGENT_CALL_CENTERS)
                                                            .Include(s => s.DEF_AGENT)
                                                            .Include(s => s.INTERVIEW_DETAILS)
                                                            .Where(i => i.Id == interviewID).SingleOrDefault();


                        AgentID = agent_interview.DEF_AGENT.ID; 

                    }
                    else if (key.Equals("call_centers_checkbox"))
                    {
                        var cc_list = formCollection[key];

                        cc_id_list = Array.ConvertAll(cc_list.Split(','), int.Parse);


                        foreach (int cc_id in cc_id_list) {
                            // update the interview assigned call centers .


                            INTERVIEW_AGENT_CALL_CENTERS agent_call_center = new INTERVIEW_AGENT_CALL_CENTERS();
                            agent_call_center.AGENT_INTERVIEW = agent_interview;
                            agent_call_center.DEF_CC = db.DEF_CC.Where(s => s.ID == cc_id).SingleOrDefault();
                            agent_call_center.TRAINING_STATUS = db.TRAINING_STATUS.Single(t => t.Name.Equals("NA"));

                            agent_interview.INTERVIEW_AGENT_CALL_CENTERS.Add(agent_call_center);

                        }
                    }
                    

                    await db.SaveChangesAsync();
                  
                }
                
            }

            return RedirectToAction("ViewAgentInterview", agent_interview);

        }


        public ActionResult ViewAgentInterview(int ? agentID)
        {
            // reload the entity to recover restored related objects
            AGENT_INTERVIEW interview = db.AGENT_INTERVIEW.Single(i => i.AgentId == agentID);

            return View("ViewAgentInterview", interview);
        }


        public ActionResult ViewInterviewDetails(AGENT_INTERVIEW interview)
        {
            // reload the entity to recover restored related objects
            interview = db.AGENT_INTERVIEW.Single(i => i.Id == interview.Id);

            return View("ViewAgentInterview", interview);
        }



        public ActionResult AdminWelcomePage()
        {
            
            return View();
        }


        public ActionResult ActiveAgents(int? pageNumber)
        {
            var result = db.Database
                .SqlQuery<sp_active_agents_Result>("sp_active_agents")
                .ToList().ToPagedList(pageNumber ?? 1, 10); ;


            return View(result);
        }

        public ActionResult CandidateAgents(int? pageNumber)
        {

            var result = db.Database
             .SqlQuery<sp_candidate_agents_Result>("sp_candidate_agents")
             .ToList().ToPagedList(pageNumber ?? 1, 10); ;


            return View(result);
        }

        public ActionResult TrainingAgents(int? pageNumber)
        {

            var result = db.Database
             .SqlQuery<sp_in_training_agents_Result>("sp_in_training_agents")
             .ToList().ToPagedList(pageNumber ?? 1, 10); ;


            return View(result);
        }
        public ActionResult RejectedAgents(int? pageNumber)
        {

            var result = db.Database
             .SqlQuery<sp_rejected_agents_Result>("sp_rejected_agents")
             .ToList().ToPagedList(pageNumber ?? 1, 10); ;


            return View(result);
        }














    }


    
}
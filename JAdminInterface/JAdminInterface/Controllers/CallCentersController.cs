﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JAdminInterface.Models.UCCEntities;
using JAdminInterface.Models.UCCViewModels;

namespace JAdminInterface.Controllers
{
    public class CallCentersController : Controller
    {
        private UCCEntities db = new UCCEntities();

        // GET: CallCenters
        public ActionResult Index()
        {
            var dEF_CC = db.DEF_CC.Include(d => d.CC_STATUS).Include(d => d.DEF_CUSTOMERS).Include(d => d.DEF_JPACKAGES).Include(d => d.DEMO_REQUESTS);
            return View(dEF_CC.ToList());
        }

        // GET: CallCenters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CC dEF_CC = db.DEF_CC.Find(id);
            if (dEF_CC == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CC);
        }

        // GET: CallCenters/Create
        public ActionResult Create()
        {
            ViewBag.Status_Id = new SelectList(db.CC_STATUS, "ID", "Name");
            ViewBag.TOS = new SelectList(db.CC_TOS, "TOS_ID", "TOS_NAME");
            ViewBag.CustomerID = new SelectList(db.DEF_CUSTOMERS, "ID", "Name");
            ViewBag.PackageID = new SelectList(db.DEF_JPACKAGES, "ID", "Name");
            ViewBag.DemoRequestID = new SelectList(db.DEMO_REQUESTS, "ID", "ID");
            ViewBag.Industry = new SelectList(db.Industries, "ID", "Name");
            return View();
        }

        // POST: CallCenters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DID,Industry,CustomerID,Creation_time,TOS,PackageID,Name,FurtherDetails,Status_Id,DemoRequestID")] DEF_CC dEF_CC)
        {
            if (ModelState.IsValid)
            {
                db.DEF_CC.Add(dEF_CC);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Status_Id = new SelectList(db.CC_STATUS, "ID", "Name", dEF_CC.Status_Id);
            ViewBag.TOS = new SelectList(db.CC_TOS, "TOS_ID", "TOS_NAME", dEF_CC.TOS);
            ViewBag.CustomerID = new SelectList(db.DEF_CUSTOMERS, "ID", "Name", dEF_CC.CustomerID);
            ViewBag.PackageID = new SelectList(db.DEF_JPACKAGES, "ID", "Name", dEF_CC.PackageID);
            ViewBag.DemoRequestID = new SelectList(db.DEMO_REQUESTS, "ID", "ID", dEF_CC.DemoRequestID);
            ViewBag.Industry = new SelectList(db.Industries, "ID", "Name", dEF_CC.Industry);
            return View(dEF_CC);
        }

        // GET: CallCenters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CC dEF_CC = db.DEF_CC.Find(id);
            if (dEF_CC == null)
            {
                return HttpNotFound();
            }
            ViewBag.Status_Id = new SelectList(db.CC_STATUS, "ID", "Name", dEF_CC.Status_Id);
            ViewBag.TOS = new SelectList(db.CC_TOS, "TOS_ID", "TOS_NAME", dEF_CC.TOS);
            ViewBag.CustomerID = new SelectList(db.DEF_CUSTOMERS, "ID", "Name", dEF_CC.CustomerID);
            ViewBag.PackageID = new SelectList(db.DEF_JPACKAGES, "ID", "Name", dEF_CC.PackageID);
            ViewBag.DemoRequestID = new SelectList(db.DEMO_REQUESTS, "ID", "ID", dEF_CC.DemoRequestID);
            ViewBag.Industry = new SelectList(db.Industries, "ID", "Name", dEF_CC.Industry);
            return View(dEF_CC);
        }

        // POST: CallCenters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DID,Industry,CustomerID,Creation_time,TOS,PackageID,Name,FurtherDetails,Status_Id,DemoRequestID")] DEF_CC dEF_CC)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dEF_CC).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Status_Id = new SelectList(db.CC_STATUS, "ID", "Name", dEF_CC.Status_Id);
            ViewBag.TOS = new SelectList(db.CC_TOS, "TOS_ID", "TOS_NAME", dEF_CC.TOS);
            ViewBag.CustomerID = new SelectList(db.DEF_CUSTOMERS, "ID", "Name", dEF_CC.CustomerID);
            ViewBag.PackageID = new SelectList(db.DEF_JPACKAGES, "ID", "Name", dEF_CC.PackageID);
            ViewBag.DemoRequestID = new SelectList(db.DEMO_REQUESTS, "ID", "ID", dEF_CC.DemoRequestID);
            ViewBag.Industry = new SelectList(db.Industries, "ID", "Name", dEF_CC.Industry);
            return View(dEF_CC);
        }

        // GET: CallCenters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CC dEF_CC = db.DEF_CC.Find(id);
            if (dEF_CC == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CC);
        }

        // POST: CallCenters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DEF_CC dEF_CC = db.DEF_CC.Find(id);
            db.DEF_CC.Remove(dEF_CC);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult EditCallCenter(int ccID, int? CustomerID)
        {
            IEnumerable<DEF_CC> cc ;


            if (ccID != -1)
            {
                cc = db.DEF_CC.Where(c => c.ID == ccID);
                ViewBag.CustomerID = cc.First().DEF_CUSTOMERS.ID;
            }
            else {
                cc = new HashSet<DEF_CC>();
                ViewBag.CustomerID = CustomerID;
            }

        
            
            ViewBag.Status_Id = new SelectList(db.CC_STATUS, "ID", "Name");
            ViewBag.TOS = new SelectList(db.CC_TOS, "TOS_ID", "TOS_NAME");
            
            ViewBag.DEF_JPACKAGES = new SelectList(db.DEF_JPACKAGES, "ID", "Name");
            ViewBag.DemoRequestID = new SelectList(db.DEMO_REQUESTS, "ID", "ID");
            ViewBag.Industry = new SelectList(db.Industries, "ID", "Name");

            

            List<DefSkillsSelectItem> insdustriesCheckList = new List<DefSkillsSelectItem>();

            var industries = db.DefSkillsViews.Where(s => s.CategoryName.Equals("Industry"));

            foreach (DefSkillsView view in industries)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                insdustriesCheckList.Add(skillItem);
            }


            var skills = db.DefSkillsViews;
            List<DefSkillsSelectItem> skillsCheckList = new List<DefSkillsSelectItem>();
            foreach (DefSkillsView view in skills)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                skillsCheckList.Add(skillItem);
            }

            ViewBag.Skills = skillsCheckList;
            return View(cc);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

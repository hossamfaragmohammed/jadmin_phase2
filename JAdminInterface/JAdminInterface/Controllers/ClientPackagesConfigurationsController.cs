﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JAdminInterface.Models.UCCEntities;

namespace JAdminInterface.Controllers
{
    public class ClientPackagesConfigurationsController : Controller
    {
        private UCCEntities db = new UCCEntities();

        // GET: ClientPackagesConfigurations
        public async Task<ActionResult> Index()
        {
            return View(await db.DEF_JPACKAGES.ToListAsync());
        }

        // GET: ClientPackagesConfigurations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_JPACKAGES dEF_JPACKAGES = await db.DEF_JPACKAGES.FindAsync(id);
            if (dEF_JPACKAGES == null)
            {
                return HttpNotFound();
            }
            return View(dEF_JPACKAGES);
        }

        // GET: ClientPackagesConfigurations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClientPackagesConfigurations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name")] DEF_JPACKAGES dEF_JPACKAGES)
        {
            if (ModelState.IsValid)
            {
                db.DEF_JPACKAGES.Add(dEF_JPACKAGES);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dEF_JPACKAGES);
        }

        // GET: ClientPackagesConfigurations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_JPACKAGES dEF_JPACKAGES = await db.DEF_JPACKAGES.FindAsync(id);
            if (dEF_JPACKAGES == null)
            {
                return HttpNotFound();
            }
            return View(dEF_JPACKAGES);
        }

        // POST: ClientPackagesConfigurations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name")] DEF_JPACKAGES dEF_JPACKAGES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dEF_JPACKAGES).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(dEF_JPACKAGES);
        }

        // GET: ClientPackagesConfigurations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_JPACKAGES dEF_JPACKAGES = await db.DEF_JPACKAGES.FindAsync(id);
            if (dEF_JPACKAGES == null)
            {
                return HttpNotFound();
            }
            return View(dEF_JPACKAGES);
        }

        // POST: ClientPackagesConfigurations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DEF_JPACKAGES dEF_JPACKAGES = await db.DEF_JPACKAGES.FindAsync(id);
            db.DEF_JPACKAGES.Remove(dEF_JPACKAGES);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JAdminInterface.Models.UCCEntities;
using PagedList;

namespace JAdminInterface.Controllers
{
    public class ClientHomeController : Controller
    {
        private UCCEntities db = new UCCEntities();

        // GET: ClientHome
        public ActionResult Index()
        {
            var dEF_CUSTOMERS = db.DEF_CUSTOMERS.
                                   Include(d => d.AspNetUser).
                                   Include(d => d.CUSTOMER_STATUS).
                                   Include(d => d.DEF_CC).
                                   Include(d => d.DEF_CC);
            return View(dEF_CUSTOMERS.ToList());
        }

        // GET: ClientHome/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CUSTOMERS = db.DEF_CUSTOMERS.Find(id);
            if (dEF_CUSTOMERS == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CUSTOMERS);
        }

        // GET: ClientHome/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName");
            return View();
        }

        // POST: ClientHome/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Rating,UserID,Status,Mobile,Address,Website,Email")] DEF_CUSTOMERS dEF_CUSTOMERS)
        {
            if (ModelState.IsValid)
            {
                db.DEF_CUSTOMERS.Add(dEF_CUSTOMERS);
                db.SaveChanges();
                return RedirectToAction("ClientsExplorer", "ManageCLient");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CUSTOMERS.UserID);
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName", dEF_CUSTOMERS.Status);
            return View(dEF_CUSTOMERS);
        }

        // GET: ClientHome/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CUSTOMERS = db.DEF_CUSTOMERS.Find(id);
            if (dEF_CUSTOMERS == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CUSTOMERS.UserID);
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName", dEF_CUSTOMERS.Status);
            return View(dEF_CUSTOMERS);
        }

        // POST: ClientHome/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Rating,UserID,Status,Mobile,Address,Website,Email")] DEF_CUSTOMERS dEF_CUSTOMERS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dEF_CUSTOMERS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CUSTOMERS.UserID);
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName", dEF_CUSTOMERS.Status);
            return View(dEF_CUSTOMERS);
        }

        // GET: ClientHome/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CUSTOMERS = db.DEF_CUSTOMERS.Find(id);
            if (dEF_CUSTOMERS == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CUSTOMERS);
        }

        // POST: ClientHome/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DEF_CUSTOMERS dEF_CUSTOMERS = db.DEF_CUSTOMERS.Find(id);
            db.DEF_CUSTOMERS.Remove(dEF_CUSTOMERS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AgentCallCentersList(int? pageNumber)
        {
            var customerCCList = db.DEF_CUSTOMERS.Single(c => c.ID == 1).DEF_CC.ToList().ToPagedList(pageNumber ?? 1, 8);
            return View(customerCCList);

        }

       

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

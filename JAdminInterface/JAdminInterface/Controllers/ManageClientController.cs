﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JAdminInterface.Models.UCCEntities;
using PagedList;
using JAdminInterface.Models;
using JAdminInterface.Models.UCCViewModels;
using Newtonsoft.Json;
using JAdminInterface.Models.AltitudeIntegration;

namespace JAdminInterface.Controllers
{
    public class ManageClientController : Controller
    {
        private UCCEntities db = new UCCEntities();

        // GET: ManageClient
        public async Task<ActionResult> Index()
        {
            var dEF_CLIENT = db.DEF_CUSTOMERS.Include(d => d.AspNetUser);
            return View(await dEF_CLIENT.ToListAsync());
        }

        // GET: ManageClient/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CLIENT = await db.DEF_CUSTOMERS.FindAsync(id);
            if (dEF_CLIENT == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CLIENT);
        }

        // GET: ManageClient/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: ManageClient/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Rating,UserID")] DEF_CUSTOMERS dEF_CLIENT)
        {
            if (ModelState.IsValid)
            {
                db.DEF_CUSTOMERS.Add(dEF_CLIENT);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CLIENT.UserID);
            return View(dEF_CLIENT);
        }

        // GET: ManageClient/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CLIENT = await db.DEF_CUSTOMERS.FindAsync(id);
            if (dEF_CLIENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CLIENT.UserID);
            return View(dEF_CLIENT);
        }

        // POST: ManageClient/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Rating,UserID")] DEF_CUSTOMERS dEF_CLIENT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dEF_CLIENT).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email", dEF_CLIENT.UserID);
            return View(dEF_CLIENT);
        }

        // GET: ManageClient/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEF_CUSTOMERS dEF_CLIENT = await db.DEF_CUSTOMERS.FindAsync(id);
            if (dEF_CLIENT == null)
            {
                return HttpNotFound();
            }
            return View(dEF_CLIENT);
        }

        // POST: ManageClient/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DEF_CUSTOMERS dEF_CLIENT = await db.DEF_CUSTOMERS.FindAsync(id);
            db.DEF_CUSTOMERS.Remove(dEF_CLIENT);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }


        public ActionResult ManageClientHome()
        {
            return View();
        }


        public ActionResult DemoRequests(int? pageNumber)
        {
            var demoRequests = db.sp_cc_requests().ToList().ToPagedList(pageNumber ?? 1, 8);
            return View(demoRequests);

        }
        /*  public ActionResult ManageDemoRequest(int DemoRequestID)
          {
              DEMO_REQUESTS demoRequest = db.DEMO_REQUESTS.Where(d => d.ID == DemoRequestID).Include(d => d.DEF_JPACKAGES).Include(d => d.DEF_CUSTOMERS).SingleOrDefault();

              return View(demoRequest);
          }
          */

        public string EditClient(int customerID, string customerName, string customerWebsite, string customerAddress, int customerRating, int customerStatus, string customerMobile) {
            DEF_CUSTOMERS client = db.DEF_CUSTOMERS.Find(customerID);
            client.Name = customerName;
            client.Website = customerWebsite;
            client.Rating = customerRating;
            client.Status = customerStatus;
            client.Address = customerAddress;
            client.Mobile = customerMobile;

            db.SaveChanges();

            return "1";
        }


        public ActionResult ManageDemoRequest(int DemoRequestID, int CustomerID)
        {

            DEMO_REQUESTS demoRequest = db.DEMO_REQUESTS.Where(d => d.ID == DemoRequestID).Include(d => d.DEF_JPACKAGES).Include(d => d.DEF_CUSTOMERS).Include(d => d.DEF_CC).SingleOrDefault();



            ViewBag.CustomerID = CustomerID;

            DEF_CUSTOMERS customer = db.DEF_CUSTOMERS.Single(c => c.ID == CustomerID);

            var industries = db.DefSkillsViews.Where(s => s.CategoryName.Equals("Industry"));

            List<DefSkillsSelectItem> insdustriesCheckList = new List<DefSkillsSelectItem>();

            foreach (DefSkillsView view in industries)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                insdustriesCheckList.Add(skillItem);
            }

            ViewBag.Industry = new SelectList(insdustriesCheckList, "ID", "SkillName");

            var TOSs = db.CC_TOS;

            ViewBag.TOS = new SelectList(TOSs, "TOS_ID", "TOS_Name");

            var clientPackages = db.DEF_JPACKAGES;

            ViewBag.DEF_JPACKAGES = new SelectList(clientPackages, "ID", "Name");

            var skills = db.DefSkillsViews;
            List<DefSkillsSelectItem> skillsCheckList = new List<DefSkillsSelectItem>();
            foreach (DefSkillsView view in skills)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                skillsCheckList.Add(skillItem);
            }

            ViewBag.Skills = skillsCheckList;

            ViewBag.Customer = customer;
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName", customer.Status);

            return View("ManageDemoRequest", demoRequest);
        }


        [HttpPost]
        public ActionResult ManageDemoRequest(int? CustomerId, int DEF_JPACKAGES, string DID, int Industry, int TOS)
        {
            var industries = db.DefSkillsViews.Where(s => s.CategoryName.Equals("Industry"));

            List<DefSkillsSelectItem> insdustriesCheckList = new List<DefSkillsSelectItem>();

            foreach (DefSkillsView view in industries)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                insdustriesCheckList.Add(skillItem);
            }

            ViewBag.Industry = new SelectList(insdustriesCheckList, "ID", "SkillName");

            var TOSs = db.CC_TOS;

            ViewBag.TOS = new SelectList(TOSs, "TOS_ID", "TOS_Name");

            var clientPackages = db.DEF_JPACKAGES;

            ViewBag.DEF_JPACKAGES = new SelectList(clientPackages, "ID", "Name");

            List<DEF_SKILLS> skills = db.DEF_SKILLS.ToList();
            List<DefSkillsSelectItem> skillsCheckList = new List<DefSkillsSelectItem>();
            foreach (DefSkillsView view in industries)
            {
                DefSkillsSelectItem skillItem = new DefSkillsSelectItem(view);
                skillsCheckList.Add(skillItem);
            }

            ViewBag.Skills = skillsCheckList;
            DEF_CUSTOMERS customer = db.DEF_CUSTOMERS.Single(c => c.ID == CustomerId);
            ViewBag.Customer = customer;
            ViewBag.Status = new SelectList(db.CUSTOMER_STATUS, "CustomerStatusID", "CustomerStatusName", customer.Status);

            return View();
        }

        public string CreateCallCenter(int CustomerID, int PackageID, string DID, int Industry, int TOS, int? CC_ID, string Name, string FurtherDetails, int? DemoRequestID)
        {
            int ccID = 0;
            if (CC_ID == -1)
            {
                // create new cc for the customer ...
                DEF_CC new_cc = new DEF_CC();
                new_cc.Creation_time = DateTime.Now;
                new_cc.CustomerID = CustomerID;
                new_cc.Industry = Industry;
                new_cc.DID = DID;
                new_cc.TOS = TOS;
                new_cc.PackageID = PackageID;
                new_cc.Name = Name;
                new_cc.FurtherDetails = FurtherDetails;
                new_cc.DemoRequestID = DemoRequestID;
                db.DEF_CUSTOMERS.SingleOrDefault(c => c.ID == CustomerID).DEF_CC.Add(new_cc);

                

                db.SaveChanges();
                ccID = new_cc.ID;
            }
            else {
                ccID = CC_ID.Value;
                DEF_CC callCenter = db.DEF_CC.Single(s => s.ID == CC_ID);

                callCenter.Creation_time = DateTime.Now;
                callCenter.CustomerID = CustomerID;
                callCenter.Industry = Industry;
                callCenter.DID = DID;
                callCenter.TOS = TOS;
                callCenter.PackageID = PackageID;
                callCenter.Name = Name;
                callCenter.FurtherDetails = FurtherDetails;

                db.SaveChanges();
            }
            return ccID + "";
        }

        public string EditCallCenterDetails(int cc_id)
        {

            return "1";
        }

        [HttpPost]
        public string EditCallCenterSkills(string[] selectedSkillsList, string[] mandatorySkillsList, string CallCenterID)
        {
            if (selectedSkillsList == null) return "1";

            bool mandatoryOverride = false;

            if (mandatorySkillsList == null) mandatoryOverride = true;

            foreach (string skill in selectedSkillsList) {

                int pos = -1;
                if (!mandatoryOverride) {
                    pos = Array.FindIndex<string>(mandatorySkillsList, x => x.Equals(skill));
                }

                if (pos > -1) {
                    Console.WriteLine(pos);
                }


                int skillID = int.Parse(skill);
                int ccID = int.Parse(CallCenterID);

                CC_SKILL cc_skill = db.CC_SKILL.Where(s => s.SkillId == skillID && s.CC_ID == ccID).FirstOrDefault();

                if (cc_skill == null) {
                    cc_skill = new CC_SKILL();
                    cc_skill.CC_ID = int.Parse(CallCenterID);
                    cc_skill.SkillId = int.Parse(skill);
                }

                cc_skill.IsMandatory = pos > -1 ? 1 : 0;

                DEF_CC cc = db.DEF_CC.SingleOrDefault(c => c.ID == cc_skill.CC_ID);

                cc.CC_SKILL.Add(cc_skill);

                db.SaveChanges();
            }

            return "1";
        }

        public void UpdateDemoRequestStatus(int demoRequestID, string newStatus) {
            db.DEMO_REQUESTS.SingleOrDefault(d => d.ID == demoRequestID).DemoRequestStatu =
                db.DemoRequestStatus.SingleOrDefault(s => s.DemoRequestStatusName.Equals(newStatus));
            db.SaveChanges();
        }

        [HttpPost]
        public string EditCallCenterPreferences(string[] ConfigItems, string[] SortTypes, string[] Weights, string CallCenterID)
        {
            int callCenterID = int.Parse(CallCenterID);

            DEF_CC callCenter = db.DEF_CC.SingleOrDefault(c => c.ID == callCenterID);

            int i = 0;
            foreach (string configItem in ConfigItems) {

                CC_CRITERIA_CONFIG newConfigCriteria = db.CC_CRITERIA_CONFIG.SingleOrDefault(c => c.CC_ID == callCenterID && c.FilterBy.Equals(configItem));
                if (newConfigCriteria == null) {
                    newConfigCriteria = new CC_CRITERIA_CONFIG();
                }
                newConfigCriteria.FilterBy = configItem;
                newConfigCriteria.Sort_type = SortTypes[i];
                newConfigCriteria.weight = int.Parse(Weights[i]);

                callCenter.CC_CRITERIA_CONFIG.Add(newConfigCriteria);

                i++;
            }
            db.SaveChanges();

            return "1";
        }


        public ActionResult ClientsExplorer(int? pageNumber) {
            var customers = db.DEF_CUSTOMERS.Where(c => c.ID != -1).ToList().ToPagedList(pageNumber ?? 1, 8);
            return View(customers);
        }

        public ActionResult Test()
        {
            return View();
        }

        public ActionResult AdminDashboard()
        {
           /* int campaignID = CampaignIntegration.Instance.createCampaign("UCC_XYZ3");

            campaignID = CampaignIntegration.Instance.createCampaign("UCC_XYZ4");
            */
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    
    public partial class SP_LIST_TEMPLATES_PLACEHOLDER_Result
    {
        public Nullable<int> Tepmlate_ID { get; set; }
        public Nullable<int> Placeholder_ID { get; set; }
        public string Placeholder_Name { get; set; }
    }
}

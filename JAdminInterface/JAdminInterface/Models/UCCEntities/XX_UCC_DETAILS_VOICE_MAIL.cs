//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class XX_UCC_DETAILS_VOICE_MAIL
    {
        public int RowId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Language { get; set; }
        public string SelectedOption { get; set; }
        public string VoiceMailURL { get; set; }
    }
}

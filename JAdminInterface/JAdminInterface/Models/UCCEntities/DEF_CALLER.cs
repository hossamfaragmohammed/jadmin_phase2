//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DEF_CALLER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEF_CALLER()
        {
            this.INTERACTION_HISTORY = new HashSet<INTERACTION_HISTORY>();
        }
    
        public int ID { get; set; }
        public string MobNo { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Rating { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERACTION_HISTORY> INTERACTION_HISTORY { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CLIENT_CALLS_BALANCES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CLIENT_CALLS_BALANCES()
        {
            this.CLIENT_CALLS_BALANCE_LINES = new HashSet<CLIENT_CALLS_BALANCE_LINES>();
        }
    
        public int BalanceID { get; set; }
        public int ClientPackageID { get; set; }
        public int NumberOfCalls { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLIENT_CALLS_BALANCE_LINES> CLIENT_CALLS_BALANCE_LINES { get; set; }
    }
}

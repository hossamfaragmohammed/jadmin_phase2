//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class XX_UCC_IVR_TRANSFER_DATA
    {
        public int RowID { get; set; }
        public long nInteractionID { get; set; }
        public long nSessionID { get; set; }
        public string tSourceChannel { get; set; }
        public string CallKey { get; set; }
        public string IVRExt { get; set; }
        public Nullable<int> nEasyCode { get; set; }
        public string tCallerNumber { get; set; }
        public Nullable<System.DateTime> CalledMoment { get; set; }
        public System.DateTime RecordMoment { get; set; }
        public string tSelectedLanguage { get; set; }
        public string tSelectedOption { get; set; }
        public string AgentName { get; set; }
        public Nullable<bool> RequestCallBack { get; set; }
        public Nullable<bool> RecordVoiceMail { get; set; }
    }
}

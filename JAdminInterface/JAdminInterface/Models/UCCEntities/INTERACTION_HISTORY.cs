//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class INTERACTION_HISTORY
    {
        public int ID { get; set; }
        public Nullable<int> CallerID { get; set; }
        public Nullable<int> AgentID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> CallerRating { get; set; }
        public Nullable<int> CustomerRating { get; set; }
        public Nullable<int> AgentRating { get; set; }
        public Nullable<System.DateTime> Start_time { get; set; }
        public Nullable<long> HT { get; set; }
        public Nullable<System.DateTime> End_time { get; set; }
        public Nullable<int> CallOutcomeID { get; set; }
        public Nullable<double> HT_Ratio { get; set; }
        public string userID { get; set; }
        public Nullable<bool> IsUdated { get; set; }
    
        public virtual CUST_CALL_OUTCOME CUST_CALL_OUTCOME { get; set; }
        public virtual DEF_AGENT DEF_AGENT { get; set; }
        public virtual DEF_CALLER DEF_CALLER { get; set; }
        public virtual DEF_CAMPAIGN DEF_CAMPAIGN { get; set; }
    }
}

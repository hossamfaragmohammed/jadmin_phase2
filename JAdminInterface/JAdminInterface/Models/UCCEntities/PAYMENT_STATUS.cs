//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAYMENT_STATUS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAYMENT_STATUS()
        {
            this.AGENT_CALLS_BALANCE_LINES = new HashSet<AGENT_CALLS_BALANCE_LINES>();
            this.CLIENT_BILLS = new HashSet<CLIENT_BILLS>();
            this.CLIENT_CALLS_BALANCE_LINES = new HashSet<CLIENT_CALLS_BALANCE_LINES>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AGENT_CALLS_BALANCE_LINES> AGENT_CALLS_BALANCE_LINES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLIENT_BILLS> CLIENT_BILLS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLIENT_CALLS_BALANCE_LINES> CLIENT_CALLS_BALANCE_LINES { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DEF_CUSTOMERS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEF_CUSTOMERS()
        {
            this.CLIENT_BILLS = new HashSet<CLIENT_BILLS>();
            this.DEF_CC = new HashSet<DEF_CC>();
            this.DEMO_REQUESTS = new HashSet<DEMO_REQUESTS>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Rating { get; set; }
        public string UserID { get; set; }
        public int Status { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CLIENT_BILLS> CLIENT_BILLS { get; set; }
        public virtual CUSTOMER_STATUS CUSTOMER_STATUS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEF_CC> DEF_CC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DEMO_REQUESTS> DEMO_REQUESTS { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CC_CRITERIA_CONFIG
    {
        public int ID { get; set; }
        public Nullable<int> CC_ID { get; set; }
        public string FilterBy { get; set; }
        public Nullable<int> weight { get; set; }
        public string Sort_type { get; set; }
    
        public virtual DEF_CC DEF_CC { get; set; }
    }
}

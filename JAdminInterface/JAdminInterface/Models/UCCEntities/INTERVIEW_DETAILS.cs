//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JAdminInterface.Models.UCCEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class INTERVIEW_DETAILS
    {
        public int Id { get; set; }
        public int SkillId { get; set; }
        public System.DateTime CreationDate { get; set; }
        public double Score { get; set; }
        public int InterviewId { get; set; }
    
        public virtual AGENT_INTERVIEW AGENT_INTERVIEW { get; set; }
        public virtual DEF_SKILLS DEF_SKILLS { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using Altitude.IntegrationServer.IDotNetApi;
using Altitude.IntegrationServer.DotNetApi;
using Altitude.IntegrationServer.Types;
using Altitude.IntegrationServer.Events;
using Altitude.IntegrationServer.Exceptions;
using System.Configuration;

namespace JAdminInterface.Models.AltitudeIntegration
{
    public class CampaignIntegration
    {

        private static CampaignIntegration instance = null;
        private static IIntegrationServer integrationServer;
        private static IInstance agentInstance;
        private static IAgent agent;

        private static bool isLoggedIn;

        private CampaignIntegration()
        {
        }

        public static CampaignIntegration Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CampaignIntegration();

                    login();
                }
                return instance;
            }
        }


        public static bool login() {
            string errMsg = string.Empty;


            string userName = "hossam";
            string password = "UCC@12345";

            string[] virtualDirectories = new string[] { "http://31.166.127.164/uagentweb82/" };

            try
            {
                integrationServer = IntegrationServerManager.GetIntegrationServer(virtualDirectories);

                errMsg = "";
                agentInstance = integrationServer.Login(ConfigurationManager.AppSettings["ASSISTED_SERVER"], false, userName, AuthenticationType.Uci, password, null, Environment.MachineName, true);
                agent = agentInstance.LoggedAgent;
                agent.SetLoginContext("", "");
                isLoggedIn = true;
            }
            catch (Exception ex) {
                isLoggedIn = false;
            }

            return isLoggedIn;

        }


        public  int createCampaign(string campaignName)
        {
            if (!isLoggedIn) {
                bool logInStatus = login();

                if (!logInStatus) {
                    return -1;
                }
            }



            try
            {
                try {
                    ICampaign campaign = agentInstance.GetCampaignByName("UCC_INBOUND_CSR");
                    
                    CampaignCreateRequest campaignCreateRequest = new CampaignCreateRequest();
                    campaignCreateRequest.PacingMode = PacingMode.PowerDial;
                    campaignCreateRequest.Name = campaignName;
                    campaignCreateRequest.ServiceName = "CSR";
                    campaignCreateRequest.TargetAgentType = CampaignTargetAgentType.Human;
                    campaignCreateRequest.Type = CampaignType.Inbound;
                    

                    int campaignID = agentInstance.CampaignManager.CreateCampaign(campaignCreateRequest);

                    agentInstance.CampaignManager.AssignTeam(campaignName, "UCC_Team");
                    agentInstance.CampaignManager.Start(campaignName);


                    return campaignID;
                }
                catch (Exception ex) {
                    return -1;
                }





                try
                {
                    
                    //agentInstance.Logout();
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return -1;
            }

            



        }
        

    }
}   
    
﻿using JAdminInterface.Models.UCCEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAdminInterface.Models.UCCViewModels
{
    public class RegisteredAgentsViewModel
    {
        public int AgentID { set; get; }
        public string AgentName { set; get; }
        public string AgentEmailAddress { set; get; }
        public string AgentPhoneNumber { set; get; }
        public DateTime AgentRegisterationTime { set; get;}

        public string InterviewStatus { set; get; }




    }
}
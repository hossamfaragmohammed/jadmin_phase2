﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using JAdminInterface.Models.UCCEntities;

namespace JAdminInterface.Models.UCCViewModels
{
    public class ScheduleInterviewViewModel
    {
        public DEF_AGENT agent;
        public AspNetUser users;



        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ScheduleDate { get; set; } = DateTime.Now;
    }
}
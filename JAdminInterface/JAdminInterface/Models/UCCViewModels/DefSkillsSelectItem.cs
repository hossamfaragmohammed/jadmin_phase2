﻿using JAdminInterface.Models.UCCEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAdminInterface.Models.UCCViewModels
{
    public class DefSkillsSelectItem : DefSkillsView
    {
        
        public bool isChecked { get; set; }

        public DefSkillsSelectItem(DefSkillsView aSkill) {

            this.ID = aSkill.ID;
            this.SkillDescRefID = aSkill.SkillDescRefID;
            this.CategoryCode = aSkill.CategoryCode;
            this.CategoryName = aSkill.CategoryName;
            this.SkillName = aSkill.SkillName;
            isChecked = false;
        }
    }
}
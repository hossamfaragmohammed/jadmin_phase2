﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JAdminInterface.Startup))]
namespace JAdminInterface
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿function updateCallCenterStatus(demoRequestID, status) {
    var url = "/ManageClient/UpdateDemoRequestStatus/?demoRequestID=" + demoRequestID + "&newStatus=" + encodeURIComponent(status);

    console.log(url);

    $.get(url, function (data) {
        console.log(data); 
        if (data == '1') {


        }

    });
}


$(document).ready(function () {
    $('a[href="' + this.location.pathname + '"]').parent('li').addClass('active');
    $('a[href="' + this.location.pathname + '"]').parent('li').parent('ul').parent('div').parent('div').addClass('in');
});


// Sidebar Expand/Collapse Handling//
$(function () {
    $('.panel-default').on('show.bs.collapse', function () {
        $(this).next().find('.collapse.in').collapse('hide');
        $(this).prev().find('.collapse.in').collapse('hide');
    });
});

$(function () {
    $('.editCustomerDetailsSubmit').click(function (e) {

        var customerID = $('.clientIDClass').val();
        var customerName = $('.clientNameClass').val();
        var customerRating = $('.clientRatingClass').val();
        var customerStatus = $('.clientStatusClass').val();
        var customerMobile = $('.clientMobileClass').val();
        var customerAddress = $('.clientAddressClass').val();
        var customerWebsite = $('.clientWebsiteClass').val();

        var url = "/ManageClient/EditClient/?CustomerID=" + encodeURIComponent(customerID) + "&customerName=" + encodeURIComponent(customerName) + "&customerRating=" + encodeURIComponent(customerRating) + "&customerStatus=" + encodeURIComponent(customerStatus) + "&customerMobile=" + encodeURIComponent(customerMobile) + "&customerAddress=" + encodeURIComponent(customerAddress) + "&customerWebsite=" + encodeURIComponent(customerWebsite);

        $.get(url, function (data) {
            console.log(data);
            if (data == '1') {

                $(".create_customer_record_panel").fadeOut('slow', function () {
                    $("#CallCenterRegion").fadeIn(500, 'linear');
                });
            }
            console.log(data);
        });
    });
});


$(function () {

    $('.createCallCenterSubmit').click(function (e) {

        var numberOfSteps = 3;
        var progressIncrementValue = 100.0 / numberOfSteps;
        var stepNumber = 1;
        var currentStepValue = progressIncrementValue * stepNumber;

        var CC_ID = $('#CC_ID').val();
        var customerID = $('#CustomerId').val();
        var packageID = $('#DEF_JPACKAGES').val();
        var DID = $('#DID').val();
        var Industry = $('#Industry').val();
        var TOS = $('#TOS').val();
        var Name = $('#CallCenterName').val();
        var FurtherDetails = $('#FurtherDetails').val();
        var DemoRequestID = 0;




        var demoRequestElement = document.getElementById("DemoRequestID");

        var url;
        if (demoRequestElement) {

            DemoRequestID = $('#DemoRequestID').val();
            url = "/ManageClient/CreateCallCenter/?CustomerID=" + customerID + "&PackageID=" + packageID + "&DID=" + DID + "&Industry=" + Industry + "&TOS=" + TOS + "&CC_ID=" + CC_ID + "&Name=" + Name + "&FurtherDetails=" + FurtherDetails + "&DemoRequestID=" + DemoRequestID;
            updateCallCenterStatus(DemoRequestID, 'In Progress')

        } else {
            console.log('step 2');

            url = "/ManageClient/CreateCallCenter/?CustomerID=" + customerID + "&PackageID=" + packageID + "&DID=" + DID + "&Industry=" + Industry + "&TOS=" + TOS + "&CC_ID=" + CC_ID + "&Name=" + Name + "&FurtherDetails=" + FurtherDetails;

        }



        $.get(url, function (data) {
            console.log(data);
            if (data != '-1') {

                console.log('data from create call center' + data);


                $(".create_call_center_panel").fadeOut('slow', function () {
                    $(".skillsRegion").fadeIn(500, 'linear');
                });

                currentStepValue = progressIncrementValue * stepNumber;
                $(".client_cc_progressbar").attr('aria-valuenow', currentStepValue);
                $(".client_cc_progressbar").css('width', currentStepValue + '%');

                document.getElementById("CC_ID").value = data;



            }
            console.log(data);
        });


    });


    $('.editCallCenterSkillsSubmit').click(function (e) {

        var numberOfSteps = 3;
        var progressIncrementValue = 100.0 / numberOfSteps;
        var stepNumber = 2;
        var currentStepValue = progressIncrementValue * stepNumber;


        var selectedSkills = new Array();
        var mandatorySkills = new Array();
        var CC_ID = $('#CC_ID').val();

        $("input:checkbox[name=selectedSkills]:checked").each(function () {
            selectedSkills.push($(this).val());

        });


        $("input:checkbox[name=mandatorySkills]:checked").each(function () {
            mandatorySkills.push($(this).val());
        });

        var checkedSkills = { selectedSkillsList: selectedSkills, mandatorySkillsList: mandatorySkills, CallCenterID: CC_ID }
        testJSON = JSON.stringify(checkedSkills);

        var payload = '"{jsonString:\"' + testJSON + '\"}"';

        $.post({
            url: "/ManageClient/EditCallCenterSkills",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: testJSON,
            success: function (response) {
                console.log(response);


                $(".skillsRegion").fadeOut('slow', function () {
                    $(".matching_criteria_preferences_panel").fadeIn(500, 'linear');
                });

                currentStepValue = progressIncrementValue * stepNumber;
                $(".client_cc_progressbar").attr('aria-valuenow', currentStepValue);
                $(".client_cc_progressbar").css('width', currentStepValue + '%');
            }
        });

    });

    $('.matchingCriteriaPreferencesSubmit').click(function (e) {

        var numberOfSteps = 3;
        var progressIncrementValue = 100.0 / numberOfSteps;
        var stepNumber = 3;
        var currentStepValue = progressIncrementValue * stepNumber;

        var CC_ID = $('#CC_ID').val();


        var AgentRatingSortType = $("#AgentRatingSelect option:selected").val();
        var HandlingTimeSortType = $("#HandlingTimeSelect option:selected").val();
        var AgentScoreSortType = $("#AgentScoreSelect option:selected").val();
        var OptionalSkillsSortType = $("#OptionalSkillsSelect option:selected").val();

        var AgentRatingWeight = $("#AgentRatingWeight").val();
        var HandlingTimgWeight = $("#HandlingTimeWeight").val();
        var AgentScoreWeight = $("#AgentScoreWeight").val();
        var OptionalSkillsWeight = $("#OptionalSkillsWeight").val();



        var configItemsArr = new Array('AgentRating', 'HandlingTime', 'AgentScore', 'OptionalSkills');
        var sortTypesArr = new Array(AgentRatingSortType, HandlingTimeSortType, AgentScoreSortType, OptionalSkillsSortType);
        var weightsArr = new Array(AgentRatingWeight, HandlingTimgWeight, AgentScoreWeight, OptionalSkillsWeight);

        var postDataArr = {
            ConfigItems: configItemsArr,
            SortTypes: sortTypesArr,
            Weights: weightsArr,
            CallCenterID: CC_ID
        };


        var DemoRequestID = 0;

        var demoRequestElement = document.getElementById("DemoRequestID");

        var url;

        var jsonData = JSON.stringify(postDataArr);

        var url = "/ManageClient/EditCallCenterPreferences";


        $.post({
            url: url,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: jsonData,
            success: function (response) {

                if (demoRequestElement) {
                    DemoRequestID = $('#DemoRequestID').val();
                    updateCallCenterStatus(DemoRequestID, 'Closed')

                }

                $(".matching_criteria_preferences_panel").fadeOut('slow', function () {
                    $(".sucessConfirmationRegion").fadeIn(500, 'linear');
                });

                currentStepValue = progressIncrementValue * stepNumber;
                $(".client_cc_progressbar").attr('aria-valuenow', currentStepValue);
                $(".client_cc_progressbar").css('width', currentStepValue + '%');


            }
        });

    });





});


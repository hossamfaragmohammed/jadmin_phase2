﻿using JAdminInterface.Models.UCCEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JAgent.Shared
{
    public class UserHelper
    {


        private static UserHelper instance;

        private UserHelper() { }


        public static UserHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserHelper();
                }
                return instance;
            }
        }


        public static int IsUserInRole(string userID, string roleName)
        {
           

            UCCEntities db = new UCCEntities();

            string sql = "select dbo.IsUserInRole(@userid_param, @rolename_param)";


            db.Database.Log = Console.Write;

            var parameters = new[]{
                        new SqlParameter("userid_param", userID),
                        new SqlParameter("rolename_param", roleName) };

            int result = db.Database.SqlQuery<int>(sql, parameters).Single();

            return result;

        }


    }




}
﻿using JAdminInterface.Models.UCCEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace JAdminInterface.Shared
{
    public class EmailNotificationSender
    {
        private static EmailNotificationSender instance;

        private EmailNotificationSender() { }

        public static EmailNotificationSender Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmailNotificationSender();
                }
                return instance;
            }
        }



        public static void SendMail(string recepients, string copy_recepients, string subject, string body, string importance)
        {

            UCCEntities db = new UCCEntities();

            string sql = "exec sp_ucc_send_email  @recipients = @recipients_param, @copy_recipients = @copy_recipients_param, @subject = @subject_param, @body = @body_param, @importance = @importance_param";


            db.Database.Log = Console.Write;

            var parameters = new[]{
                        new SqlParameter("recipients_param", recepients),
                        new SqlParameter("copy_recipients_param", copy_recepients),
                        new SqlParameter("subject_param", subject),
                        new SqlParameter("body_param", body),
                        new SqlParameter("importance_param", importance) };

            db.Database.SqlQuery<object>(sql, parameters).ToList();
        }


    }
}